package org.Unterricht.UMLUebung.Bibliothekssystem;

import java.util.Date;

public class Exemplar {
    private Date ausleihDatum;
    private String signatur;

    private Leser ausleiher;
    private Buch buch;

    public Exemplar(String signatur, Buch buch) {
        this.signatur = signatur;
        this.buch = buch;
    }

    public boolean istAusgeliehen() {
        return ausleiher != null;
    }

    public Date spaetestesRueckgabeDatum() {
        //                                                           hours min secs minsecs
        return new Date(ausleihDatum.getTime() + Buch.getAusleihDauer()*24*60*60*1000);
    }

    public int ausleihen(Leser leser) {
        if (leser.getExemplare().size() >= Leser.getMaxAusleihen()) {
            return 2;
        } else if (istAusgeliehen()) {
            return 1;
        } else {
            leser.addAusleihe(this);
            setAusleiher(leser);
            return 0;
        }
    }

    public int zurueckgeben() {
        if (!istAusgeliehen()) {
            return 2;
        } else if (spaetestesRueckgabeDatum().after(new Date())) {
            return 1;
        } else {
            ausleiher.removeAusleihe(this);
            setAusleiher(null);
            return 0;
        }
    }

    public Date getAusleihDatum() {
        return ausleihDatum;
    }

    public String getSignatur() {
        return signatur;
    }

    public Leser getAusleiher() {
        return ausleiher;
    }

    public Buch getBuch() {
        return buch;
    }

    public void setAusleiher(Leser ausleiher) {
        this.ausleiher = ausleiher;
        if (ausleiher != null) {
            this.ausleihDatum = new Date(); // is automaticly the current date
        } else ausleihDatum = null;
    }
}
