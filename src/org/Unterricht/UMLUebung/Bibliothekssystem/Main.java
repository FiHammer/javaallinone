package org.Unterricht.UMLUebung.Bibliothekssystem;


public class Main {
    public static void main(String[] args) {
        Bibliothekssystem bs = new Bibliothekssystem();
        Buch buch1 = new Buch("1", "Buch 1");
        Buch buch2 = new Buch("2", "Buch 2");
        Buch buch3 = new Buch("3", "Buch 3");

        for (int x = 0; x < 3; x++) {
            buch1.addExemplar(new Exemplar("b1" + x, buch1));
            buch2.addExemplar(new Exemplar("b2" + x, buch1));
            buch3.addExemplar(new Exemplar("b3" + x, buch1));
        }

        bs.getBuecher().add(buch1);
        bs.getBuecher().add(buch2);
        bs.getBuecher().add(buch3);

        Leser l1 = new Leser(1, "Leser 1");
        Leser l2 = new Leser(2, "Leser 1");
        Leser l3 = new Leser(3, "Leser 1");

        bs.getLeser().add(l1);
        bs.getLeser().add(l2);
        bs.getLeser().add(l3);

        bs.ausleihen(l1, buch1.getExemplare().get(2));
        bs.ausleihen(l3, buch1.getExemplare().get(1));

        System.out.println("END!");

    }
}
