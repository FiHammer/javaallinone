package org.Unterricht.UMLUebung.Bibliothekssystem;

import java.util.ArrayList;
import java.util.Date;

public class Bibliothekssystem {
    private ArrayList<Buch> buecher;
    private ArrayList<Leser> leser;

    public Bibliothekssystem() {
        buecher = new ArrayList<>();
        leser = new ArrayList<>();
    }

    public Leser sucheLeser(int lesernr) {
        for (Leser l: leser) {
            if (lesernr == l.getLesernr()) {
                return l;
            }
        }
        return null;
    }

    public int ausleihen(Leser leser, Exemplar exemplar) {
        if (leser.getExemplare().size() >= Leser.getMaxAusleihen()) {
            return 2;
        } else if (exemplar.istAusgeliehen()) {
            return 1;
        } else {
            leser.addAusleihe(exemplar);
            exemplar.setAusleiher(leser);
            return 0;
        }
    }

    public int zurueckgeben(Leser leser, Exemplar exemplar) {
        if (!exemplar.istAusgeliehen()) {
            return 2;
        } else if (exemplar.spaetestesRueckgabeDatum().after(new Date())) {
            return 1; // todo maybe ausleihen
        } else {
            leser.removeAusleihe(exemplar);
            exemplar.setAusleiher(null);
            return 0;
        }
    }


    public Exemplar sucheExemplar(String signatur) {
        for (Buch b: buecher) {
            for (Exemplar e: b.getExemplare()) {
                if (signatur.equals(e.getSignatur())) {
                    return e;
                }
            }
        }
        return null;
    }

    public ArrayList<Buch> getBuecher() {
        return buecher;
    }

    public ArrayList<Leser> getLeser() {
        return leser;
    }
}
