package org.Unterricht.UMLUebung.Bibliothekssystem;

import java.util.ArrayList;

public class Leser {
    private static int maxAusleihen = 3;
    private int lesernr;
    private String name;

    private ArrayList<Exemplar> exemplare;

    public Leser(int lesernr, String name) {
        this.lesernr = lesernr;
        this.name = name;

        exemplare = new ArrayList<>();
    }

    public boolean hatAusleihen() {
        return exemplare.size() > 0;
    }

    public void addAusleihe(Exemplar exemplar) { // no invalid check!
        exemplare.add(exemplar);
    }

    public boolean removeAusleihe(Exemplar exemplar) { // no invalid check!, can cause Exception
        return exemplare.remove(exemplar);
    }

    public static int getMaxAusleihen() {
        return maxAusleihen;
    }

    public int getLesernr() {
        return lesernr;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Exemplar> getExemplare() {
        return exemplare;
    }
}
