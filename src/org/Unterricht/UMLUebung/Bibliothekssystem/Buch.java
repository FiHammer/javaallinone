package org.Unterricht.UMLUebung.Bibliothekssystem;

import java.util.ArrayList;

public class Buch {
    private static int ausleihDauer;
    private String isbn;
    private String title;

    private ArrayList<Exemplar> exemplare;

    public Buch(String isbn, String title) {
        this.isbn = isbn;
        this.title = title;
        exemplare = new ArrayList<>();
    }

    public void addExemplar(Exemplar exemplar) {
        exemplare.add(exemplar);
    }

    public boolean istVerfuegbar() {
        for (Exemplar e: exemplare) {
            if (!e.istAusgeliehen()) return true;
        }
        return false;
    }

    public void removeExemplar(Exemplar exemplar) {
        exemplare.remove(exemplar);
    }

    public static int getAusleihDauer() {
        return ausleihDauer;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<Exemplar> getExemplare() {
        return exemplare;
    }
}
