package org.Unterricht.UMLUebung.Stundenplanverwaltung.Code;

public class Unterrichtsstunde {

	private int stunde;

	private String wochentag;

	private Fach fach;
	private Klasse klasse;

	public Fach getFach() {
		return fach;
	}

	public Klasse getKlasse() {
		return klasse;
	}

	public int getStunde() {
		return stunde;
	}
}
