package org.Unterricht.UMLUebung.Stundenplanverwaltung.Code;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * most methods missing
 */
public class Lehrkraft {

	private String anrede;
	private static int autoNr;
	private int lehrkraftNr;
	private String name;
	private String vorname;

	private ArrayList<Vertretung> vertretungen;

	public Lehrkraft(String anrede, String name, String vorname) {

	}

	public void hinzufuegenVertretung(Date d, Unterrichtsstunde s) {
		Vertretung new_vertretung = new Vertretung(d, this, s);
		ArrayList<Vertretung> new_vertretungen = new ArrayList<>();

		boolean added=false;
		for (Vertretung v: vertretungen) {
			if (v.getDatum().compareTo(d) == 0 && !added) { // same
				if (v.getVertretungsstunde().getStunde() < s.getStunde()) {  // previous
					new_vertretungen.add(v);
				} else if (v.getVertretungsstunde().getStunde() == s.getStunde()) { // same
					new_vertretungen.add(v);
					new_vertretungen.add(new_vertretung);
					added = true;
				} else { // after
					new_vertretungen.add(new_vertretung);
					new_vertretungen.add(v);
					added = true;
				}
			} else {
				new_vertretungen.add(v); // IMPORTANT, if the list changed somehow this will completely make this crash
			}
		}

		vertretungen = new_vertretungen;
	}

	public boolean hatUnterricht(Date d, int stunde) {
		return false;
	}

	public boolean hatVertretung(Date d, int stunde) {
		return false;
	}

}
