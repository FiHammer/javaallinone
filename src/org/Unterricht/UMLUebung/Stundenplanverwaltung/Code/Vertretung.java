package org.Unterricht.UMLUebung.Stundenplanverwaltung.Code;

import java.util.Date;

public class Vertretung {

	private Date datum;
	private Unterrichtsstunde vertretungsstunde;

	public Vertretung(Date d, Lehrkraft l, Unterrichtsstunde s) {
		this.vertretungsstunde = s;
	}

	public Date getDatum() {
		return datum;
	}

	public Unterrichtsstunde getVertretungsstunde() {
		return vertretungsstunde;
	}
}
