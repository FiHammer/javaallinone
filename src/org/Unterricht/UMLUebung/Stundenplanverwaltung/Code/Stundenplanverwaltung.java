package org.Unterricht.UMLUebung.Stundenplanverwaltung.Code;

import java.util.ArrayList;
import java.util.Date;

public class Stundenplanverwaltung {
    private ArrayList<Lehrkraft> lehrkraefte;

    public Stundenplanverwaltung() {
        lehrkraefte = new ArrayList<>();
    }

    public ArrayList<Lehrkraft> sucheVertretungslehrer(Date d, int stunde, Lehrkraft l) {
        ArrayList<Lehrkraft> free_people = new ArrayList<>();

        for (Lehrkraft lehr: lehrkraefte) {
            if (!lehr.hatUnterricht(d, stunde) && !lehr.hatVertretung(d, stunde)) {
                free_people.add(lehr);
            }
        }

        if (free_people.isEmpty()) return free_people;  // or use size()==0

        // prio 1: same class

        ArrayList<Lehrkraft> possi;

        possi = sucheVertretungslehrer(free_people, sucheUnterrichtsstunde(d, stunde, l).getKlasse());
        if (!possi.isEmpty()) return possi;

        // prio 2: same type

        possi = sucheVertretungslehrer(free_people, sucheUnterrichtsstunde(d, stunde, l).getFach());
        if (!possi.isEmpty()) return possi;

        return free_people;
    }

    private ArrayList<Lehrkraft> sucheVertretungslehrer(ArrayList<Lehrkraft> li, Fach f) {
        return new ArrayList<>();
    }

    private ArrayList<Lehrkraft> sucheVertretungslehrer(ArrayList<Lehrkraft> li, Klasse k) {
        return new ArrayList<>();
    }

    private Unterrichtsstunde sucheUnterrichtsstunde(Date d, int stunde, Lehrkraft l) {
        return new Unterrichtsstunde();  // tmp
    }
}
