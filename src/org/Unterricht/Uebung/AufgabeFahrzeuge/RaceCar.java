package org.Unterricht.Uebung.AufgabeFahrzeuge;

public class RaceCar extends Car {
    public RaceCar(double speed, double maxSpeed, String type) {
        super(speed, maxSpeed, type);
    }
    public RaceCar(double speed) {
        super(speed, 220, "RaceCar");
    }
    public RaceCar() {
        super(0, 220, "RaceCar");
    }
}
