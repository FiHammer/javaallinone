package org.Unterricht.Uebung.AufgabeFahrzeuge;

public abstract class Vehicle {
    private double position = 0;  // in km, one dimension
    private double speed;  // km/h
    private double maxSpeed = 0;
    public final String type;

    private int wheels = 0;

    public Vehicle(double speed, double maxSpeed, int wheels, String type) {
        this.speed = speed;
        this.maxSpeed = maxSpeed;
        this.wheels = wheels;
        this.type = type;
    }

    /**
     *
     * @param time: time in minutes
     */
    public void move(double time) {
        position += speed * (time/60);
    }

    public void setSpeed(double speed) {
        if (speed <= maxSpeed) {
            this.speed = speed;
        }  // throw an exception here?
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getPosition() {
        return position;
    }
}