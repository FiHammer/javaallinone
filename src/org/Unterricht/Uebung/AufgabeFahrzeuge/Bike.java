package org.Unterricht.Uebung.AufgabeFahrzeuge;

public class Bike extends Vehicle {
    public Bike(double speed, double maxSpeed, String type) {
        super(speed, maxSpeed, 2, type);
    }
    public Bike(double speed) {
        super(speed, 30, 2,"Bike");
    }
    public Bike() {
        super(0, 30, 2,"Bike");
    }
}