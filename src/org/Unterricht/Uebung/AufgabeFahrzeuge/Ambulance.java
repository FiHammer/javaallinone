package org.Unterricht.Uebung.AufgabeFahrzeuge;

public class Ambulance extends Car {
    private boolean emeLight;

    public Ambulance(double speed, double maxSpeed, String type) {
        super(speed, maxSpeed, type);
    }
    public Ambulance(double speed) {
        super(speed, 80, "Ambulance");
    }
    public Ambulance() {
        super(0, 80, "Ambulance");
    }

    public void changeEmeLight(boolean status) {
        emeLight = status;
    }
    public void turnEmeLightOn() {
        changeEmeLight(true);
    }
    public void turnEmeLightOff() {
        changeEmeLight(false);
    }


}
