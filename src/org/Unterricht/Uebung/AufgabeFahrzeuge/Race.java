package org.Unterricht.Uebung.AufgabeFahrzeuge;


public class Race {
    private Bike bike;
    private Car car;
    private RaceCar raceCar;
    private Ambulance ambulance;

    public Race() {
        bike = new Bike(20);
        car = new Car(150);
        raceCar = new RaceCar(200);
        ambulance = new Ambulance(80);
    }

    public void startRace() {
        bike.move(4*60);
        car.move(60);
        ambulance.move(2*60);

        for (int x=0; x<20; x++) {
            bike.move(1);
            car.move(1);
            ambulance.move(1);
            raceCar.move(1);
        }
    }

    public void showPos() {
        System.out.println("Bike: " + bike.getPosition());
        System.out.println("Car: " + car.getPosition());
        System.out.println("Ambulance: " + ambulance.getPosition());
        System.out.println("Racecar: " + raceCar.getPosition());

        Vehicle biggest;

        biggest = bike;
        if (car.getPosition() > biggest.getPosition()) {
            biggest = car;
        }
        if (ambulance.getPosition() > biggest.getPosition()) {
            biggest = car;
        }
        if (raceCar.getPosition() > biggest.getPosition()) {
            biggest = car;
        }

        Vehicle smallest;

        smallest = bike;
        if (car.getPosition() < smallest.getPosition()) {
            smallest = car;
        }
        if (ambulance.getPosition() < smallest.getPosition()) {
            smallest = ambulance;
        }
        if (raceCar.getPosition() < smallest.getPosition()) {
            smallest = raceCar;
        }

        System.out.println("\nFirst place: " + biggest.type);
        System.out.println("Last place: " + smallest.type);
    }

    public static void main(String[] args) {
        Race r = new Race();
        r.startRace();
        r.showPos();
    }
}
