package org.Unterricht.Uebung.AufgabeFahrzeuge;

public class Car extends Vehicle {
    public Car(double speed, double maxSpeed, String type) {
        super(speed, maxSpeed, 4, type);
    }
    public Car(double speed) {
        super(speed, 150, 4, "Car");
    }
    public Car() {
        super(0, 150, 4, "Car");
    }
}
