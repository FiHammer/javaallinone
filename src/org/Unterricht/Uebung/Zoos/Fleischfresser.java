package org.Unterricht.Uebung.Zoos;

public class Fleischfresser extends Tier{
    public boolean istJaeger;
    public Tierliste magenInhalt;

    public Fleischfresser(String name, double gewicht, boolean lebendig, boolean istJaeger, Zoo zoo) {
        super(name, gewicht, lebendig, zoo);
        this.istJaeger = istJaeger;
        magenInhalt = new Tierliste();
    }


    public void friss(Tier beutetier) {
        if (beutetier.getName().equals(getName())) {
            System.out.println(getName() + " kann sich nicht selbst fressen!");
            return;
        } else
        if (beutetier.getZoo() != getZoo()) {
            System.out.println(getName() + " kann nicht " + beutetier.getName() + " fressen, da es in dem Zoo " +
                    beutetier.getZoo() + " lebt!");
            return;
        } else
        if (beutetier.isLebendig()) {
            System.out.println(getName() + " kann nicht " + beutetier.getName() + " fressen, da es nicht tot ist");
            return;
        }

        System.out.println("Fresse " + beutetier.getName());

        if (beutetier instanceof Fleischfresser) {
            System.out.println("Igitt Igitt");
        }

        if (beutetier instanceof Reh) {
            Reh r = (Reh)beutetier;
            if (r.getGeweih()) {
                System.out.println("Vorsicht! Spitzes Geweih");
            } else {
                System.out.println("Hmm...schoen saftig");
            }
        }

        if (beutetier instanceof Pflanzenfresser) {
            Pflanzenfresser t = (Pflanzenfresser)beutetier;
            System.out.println("Das gefressene Tier hat gerne " + t.getLieblingsPflanze() + " gegessen.");
        }

        //System.out.println("\n\n\n" + getZoo().loescheTier(beutetier));
        magenInhalt.fuegeHinzu(beutetier);
        getZoo().loescheTier(beutetier);
    }

    public String magenInhaltAsString() {
        return magenInhalt.getNames();
    }

    public void gibMagenInhaltAus() {
        System.out.println("Der Mageninhalt von " + getName() + " ist: " + magenInhalt.getNames());
    }

    @Override
    public String toString() {
        String s;
        if (istJaeger) s = "Jaeger";
        else s = "Aasfresser";
        return super.toString() + ", ist ein " + s;
    }
}
