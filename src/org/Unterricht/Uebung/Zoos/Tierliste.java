package org.Unterricht.Uebung.Zoos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Tierliste implements Iterable{
    private ArrayList<Tier> tiere;

    public Tierliste() {
        tiere = new ArrayList<>();
    }

    private boolean existiertName(String name) {
        for (Tier t: tiere) {
            if (t.getName().equals(name)) return true;
        }
        return false;
    }

    public boolean fuegeHinzu(Tier tier) {
        if (existiertName(tier.getName())) return false;
        tiere.add(tier);
        return true;
    }

    public boolean loesche(Tier tier) {
        if (!tiere.contains(tier)) {
            return false;
        }
        tiere.remove(tier);
        return true;
    }

    public ArrayList<Tier> getTiere() {
        return tiere;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        for (Tier t: tiere) {
            s.append(t.toString()).append("\n");
        }
        return s.toString().strip();
    }

    public String getNames() {
        StringBuilder s = new StringBuilder();

        for (Tier t: tiere) {
            s.append(t.getName()).append(", ");
        }
        return s.deleteCharAt(s.lastIndexOf(",")).toString().strip();
    }

    @Override
    public Iterator iterator() {
        return tiere.iterator();
    }

    @Override
    public void forEach(Consumer action) {
        tiere.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        return tiere.spliterator();
    }
}
