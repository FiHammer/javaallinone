package org.Unterricht.Uebung.Zoos;

public class Hyaene extends Fleischfresser {
    public Hyaene(String name, double gewicht, boolean lebendig, Zoo zoo) {
        super(name, gewicht, lebendig, true, zoo);
    }

    @Override
    public String toString() {
        return super.toString() + " und ist eine Hyaene";
    }
}
