package org.Unterricht.Uebung.Zoos;

public class Hase extends Pflanzenfresser {
    private double ohrenlaenge;

    public Hase(String name, double gewicht, boolean lebendig, String lieblingsPflanze, double ohrenlaenge, Zoo zoo) {
        super(name, gewicht, lebendig, lieblingsPflanze, zoo);
        this.ohrenlaenge = ohrenlaenge;
    }

    @Override
    public String toString() {
        return super.toString() + ", ist ein Hase und hat " + ohrenlaenge + "cm lange Ohren";
    }
}
