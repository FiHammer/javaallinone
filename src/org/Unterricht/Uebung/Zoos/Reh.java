package org.Unterricht.Uebung.Zoos;

public class Reh extends Pflanzenfresser {
    private boolean geweih;

    public Reh(String name, double gewicht, boolean lebendig, String lieblingsPflanze, boolean geweih, Zoo zoo) {
        super(name, gewicht, lebendig, lieblingsPflanze, zoo);
        this.geweih = geweih;
    }

    public boolean getGeweih() {
        return geweih;
    }

    @Override
    public String toString() {
        String s;
        if (geweih) s = "ein";
        else s = "kein";

        return super.toString() + ", ist ein Reh und hat zur Zeit " + s + " Geweih";
    }
}
