package org.Unterricht.Uebung.Zoos;

public class Zoo {
    private String name;
    private Tierliste tiere;

    public Zoo(String name) {
        this.name = name;
        tiere = new Tierliste();
    }

    public boolean nimmTierAuf(Tier zootier) {
        return tiere.fuegeHinzu(zootier);
    }
    public boolean loescheTier(Tier opfer) {
        return tiere.loesche(opfer);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Der Zoo " + getName() + " beherbergt:\n" + tiere;
    }
}
