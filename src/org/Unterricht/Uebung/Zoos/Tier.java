package org.Unterricht.Uebung.Zoos;

public class Tier {
    private double gewicht;
    private String name;
    private boolean lebendig;
    private Zoo zoo;

    public Tier(String name, double gewicht, boolean lebendig, Zoo zoo) {
        this.name = name;
        this.gewicht = gewicht;
        this.lebendig = lebendig;
        this.zoo = zoo;

        zoo.nimmTierAuf(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLebendig() {
        return lebendig;
    }

    public void sterbe() {
        this.lebendig = false;
    }

    public Zoo getZoo() {
        return zoo;
    }

    public void setZoo(Zoo zoo) {
        this.zoo = zoo;
    }

    @Override
    public String toString() {
        String s;
        if (lebendig) s = "lebt";
        else s = "ist tot";

        return "Das Tier heisst " + getName() + ", wiegt " + gewicht + "kg, " + s + ", wohnt im Zoo " + zoo.getName();
    }
}
