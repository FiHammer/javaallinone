package org.Unterricht.Uebung.Zoos;

public class Pflanzenfresser extends Tier {
    private String lieblingsPflanze;

    public Pflanzenfresser(String name, double gewicht, boolean lebendig, String lieblingsPflanze, Zoo zoo) {
        super(name, gewicht, lebendig, zoo);
        this.lieblingsPflanze = lieblingsPflanze;
    }

    public String getLieblingsPflanze() {
        return lieblingsPflanze;
    }

    @Override
    public String toString() {
        return super.toString() + ", isst gerne " + lieblingsPflanze;
    }
}
