package org.Unterricht.Uebung.Zoos;

public class Wolf extends Fleischfresser{

    public Wolf(String name, double gewicht, boolean lebendig, Zoo zoo) {
        super(name, gewicht, lebendig, true, zoo);
    }

    public void erlege(Tier beutetier) {
        if (!isLebendig()) {
            System.out.println("Ich bin tot! Ich kann deswegen nicht mehr erlegen");
            return;
        } else
        if (!beutetier.isLebendig()) {
            System.out.println("Die Beute ist bereits tot und kann daher nicht von " + getName() + " erlegt werden.");
            return;
        }

        beutetier.sterbe();
    }

    @Override
    public String toString() {
        return super.toString() + " und ist ein Wolf";
    }
}
