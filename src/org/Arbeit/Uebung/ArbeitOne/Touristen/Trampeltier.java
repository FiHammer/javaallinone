package org.Arbeit.Uebung.ArbeitOne.Touristen;

public class Trampeltier extends Kamel {
    private void ladeLastAuf() {
        System.out.println("<<Last aufladen>>");
    }

    @Override
    public void trageLast() {
        ladeLastAuf();  // last aufladen
        super.trageLast();
    }
}
