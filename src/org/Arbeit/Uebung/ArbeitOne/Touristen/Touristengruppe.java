package org.Arbeit.Uebung.ArbeitOne.Touristen;

import java.util.ArrayList;

public class Touristengruppe {
    private ArrayList<Kamel> kamele;

    public Touristengruppe() {
        kamele = new ArrayList<>();
    }

    public void run() {
        kamele.clear();

        kamele.add(new Lama());

        kamele.add(new Trampeltier());
        kamele.add(new Trampeltier());
        kamele.add(new Trampeltier());

        kamele.add(new Dromedar());
        kamele.add(new Dromedar());

        for (Kamel k: kamele) {
            k.trageLast();
        }
    }

    public static void main(String[] args) {
        Touristengruppe t = new Touristengruppe();
        t.run();
    }
}
