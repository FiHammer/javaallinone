package org.Arbeit.Uebung.ArbeitOne.Touristen;

public class Lama extends Kamel {
    private void spucke() {
        System.out.println("<<flatsch>>");
    }

    @Override
    public void trageLast() {
        spucke();
        super.trageLast();
        spucke();
    }
}
