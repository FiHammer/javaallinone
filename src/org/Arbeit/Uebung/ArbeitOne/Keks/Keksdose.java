package org.Arbeit.Uebung.ArbeitOne.Keks;

import java.util.ArrayList;

public class Keksdose {
    private ArrayList<Keks> kekse;

    public Keksdose() {
        kekse = new ArrayList<>();
    }

    public double berechneGesamtEnergie() {
        double energie = 0;
        for (Keks k: kekse) {
            energie += k.gibGesamtEnergie();
        }
        return energie;
    }

    public void hinzufuegen(Keks k) {
        kekse.add(k);
    }
}
