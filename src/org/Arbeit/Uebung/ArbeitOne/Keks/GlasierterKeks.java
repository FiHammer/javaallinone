package org.Arbeit.Uebung.ArbeitOne.Keks;

public class GlasierterKeks extends Keks {
    private double glasurEnergie;

    public GlasierterKeks(double energie, double glasurEnergie) {
        super(energie);
        this.glasurEnergie = glasurEnergie;
    }

    @Override
    public double gibGesamtEnergie() {
        return super.gibGesamtEnergie() + glasurEnergie;
    }
}
