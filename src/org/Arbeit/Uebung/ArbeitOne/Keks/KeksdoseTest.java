package org.Arbeit.Uebung.ArbeitOne.Keks;

public class KeksdoseTest {
	public static void main(String[] args) {
		
		Keksdose  dose = new Keksdose();
		Keks k1,k2;
		GlasierterKeks  gk1 , gk2 , gk3;

		k1 = new Keks(400); // Keks 400 Kalorien
		k2 = new Keks(280); // Keks mit 280 Kalorien
		gk1 = new GlasierterKeks (200,66); // Keks 200 Kalorien und 66 Kalorien der Glasur
		gk2 = new GlasierterKeks (300,77);
		gk3 = new GlasierterKeks (400,88);

		dose.hinzufuegen(k1);
		dose.hinzufuegen(k2);
		dose.hinzufuegen(gk1);
		dose.hinzufuegen(gk2);
		dose.hinzufuegen(gk3);

		System.out.print("Gesamte Energie in der Dose: ");
		System.out.println("" + dose.berechneGesamtEnergie ());


	}

}
