package org.Arbeit.Uebung.ArbeitOne.Keks;

public class Keks {
    private double energie;

    public Keks(double energie) {
        this.energie = energie;
    }

    public double gibGesamtEnergie() {
        return energie;
    }
}
