package org.Arbeit.Uebung.Erbung;

public class StinkyDogHair extends DogHair {
    private boolean stinks = true;

    public StinkyDogHair(String ownerName) {
        super(ownerName);
    }
    public StinkyDogHair() {
        super();
    }

    public void smell() throws BadSmellException{
        if (stinks) {
            throw new BadSmellException(this.ownerName);
        } else {
            System.out.println("It smells ok.");
        }
    }

    public void wash() {
        stinks = false;
    }

    public void throwInTrash() {
        stinks = true;
    }

    @Override
    public void show() {
        super.show();
        if (stinks) {
            System.out.println("Ouhh, it smells really bad!");
        }
    }
}
