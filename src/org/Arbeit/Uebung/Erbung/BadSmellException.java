package org.Arbeit.Uebung.Erbung;

public class BadSmellException extends Exception {

    public BadSmellException() {
        this("");
    }

    public BadSmellException(String thanker) {
        super();
        if (!thanker.equals("")) {
            System.err.println("You died, thank " + thanker + " for his fantastic smell!");
        } else {
            System.err.println("You died!");
        }
    }
}
