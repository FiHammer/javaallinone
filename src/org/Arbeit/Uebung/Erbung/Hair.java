package org.Arbeit.Uebung.Erbung;

public class Hair {
    private int length;  // in mm

    public Hair() {
        this(0);
    }

    public Hair(int startLength) {
        this.length = startLength;
    }

    public void grow() {
        this.length++;
    }

    public void grow(int times) {
        if (times<0) return;
        for (int x=0; x<times; x++) {
            grow();
        }
    }

    public void cut() {
        if (length > 0) {
            this.length--;
        }
    }

    public void cut(int times) {
        if (times<0) return;
        for (int x=0; x<times; x++) {
            cut();
        }
    }

    public void show() {
        for (int x=0; x<length; x++) {
            System.out.print("-");
        }
        System.out.print("\n");
    }

}
