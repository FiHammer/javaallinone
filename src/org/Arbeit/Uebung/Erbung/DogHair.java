package org.Arbeit.Uebung.Erbung;

public class DogHair extends Hair {
    protected String ownerName;

    public DogHair(String ownerName) {
        this.ownerName = ownerName;
    }
    public DogHair() {
        this.ownerName = "";
    }

    public String getOwnerName() {
        return ownerName;
    }

    @Override
    public void show() {
        super.show();
        if (!ownerName.equals("")) {
            System.out.println("This dog hair comes from " + ownerName);
        }
    }
}
