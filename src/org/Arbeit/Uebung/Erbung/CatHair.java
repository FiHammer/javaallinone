package org.Arbeit.Uebung.Erbung;

public class CatHair extends Hair {
    protected String ownerName;

    public CatHair(String ownerName) {
        this.ownerName = ownerName;
    }
    public CatHair() {
        this.ownerName = "";
    }

    @Override
    public void show() {
        super.show();
        if (!ownerName.equals("")) {
            System.out.println("This cat hair comes from " + ownerName);
        }
    }
}
