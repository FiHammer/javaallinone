package org.Arbeit.Uebung.Erbung;

public class HairMain {
    public static void main(String[] args) {

        Hair h = new StinkyDogHair("Rudolf");
        h.grow();
        h.grow();
        h.show();

        // hello, from MINI PC

        StinkyDogHair sh = (StinkyDogHair)h;
        sh.wash();

        sh.grow(55);

        sh.show();

        try {
            sh.smell();
        } catch (BadSmellException bse) {
            System.out.println("You ran away");
        }

        sh.throwInTrash();
        try {
            sh.smell();
        } catch (BadSmellException bse) {
            System.out.println("You ran away");
        }

    }
}
