package org.Bsp.Iteration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class MyList implements Iterable {
    private ArrayList<String> content;

    public MyList() {
        content = new ArrayList<>();
    }

    public void add(String s) {
        content.add(s);
    }

    @Override
    public Iterator iterator() {
        return content.iterator();
    }

    @Override
    public void forEach(Consumer action) {
        content.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        return content.spliterator();
    }
}
