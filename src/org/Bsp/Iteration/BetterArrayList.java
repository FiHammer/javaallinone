package org.Bsp.Iteration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class BetterArrayList extends ArrayList {
    @Override
    public ListIterator listIterator(int index) {
        return super.listIterator(index);
    }

    @Override
    public ListIterator listIterator() {
        return super.listIterator();
    }

    @Override
    public Iterator iterator() {
        return super.iterator();
    }

    @Override
    public void forEach(Consumer action) {
        super.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        return super.spliterator();
    }
}
