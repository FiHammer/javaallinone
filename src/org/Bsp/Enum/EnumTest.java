package org.Bsp.Enum;

public class EnumTest {
    Feelings feelings;

    public EnumTest(Feelings f) {
        feelings = f;
    }

    public void test() {
        switch (feelings) {
            case HATE:
                System.out.println("No please do not hate");
            default:
                System.out.println("Ok");
        }
    }

    public static void main(String[] args) {
        EnumTest et = new EnumTest(Feelings.FRANK);
        et.test();
    }

}
